# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/expand_condition'

describe Gitlab::Triage::ExpandCondition do
  describe '.perform' do
    it 'performs with label list expansion' do
      conditions = {
        labels: ['{ apple, orange }']
      }

      labels = []

      subject.perform(conditions, [subject::List]) do |new_conditions|
        labels << new_conditions[:labels]
      end

      expect(labels).to eq(
        [
          %w[apple],
          %w[orange]
        ]
      )
    end

    it 'performs with label sequence expansion' do
      conditions = {
        labels: %w[missed:{0..1} missed:{2..3}]
      }

      labels = []

      subject.perform(conditions, [subject::Sequence]) do |new_conditions|
        labels << new_conditions[:labels]
      end

      expect(labels).to eq(
        [
          %w[missed:0 missed:2],
          %w[missed:0 missed:3],
          %w[missed:1 missed:2],
          %w[missed:1 missed:3]
        ]
      )
    end

    it 'performs with label list and sequence expansion' do
      conditions = {
        labels: %w[{apple,orange} P{1..2} {grape,banana}]
      }

      labels = []

      subject.perform(conditions) do |new_conditions|
        labels << new_conditions[:labels]
      end

      # TODO: It'll be nice if we could always expand from right to left
      expect(labels).to eq(
        [
          %w[apple P1 grape],
          %w[apple P2 grape],
          %w[apple P1 banana],
          %w[apple P2 banana],
          %w[orange P1 grape],
          %w[orange P2 grape],
          %w[orange P1 banana],
          %w[orange P2 banana]
        ]
      )
    end
  end
end
