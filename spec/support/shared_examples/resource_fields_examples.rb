# frozen_string_literal: true

RSpec.shared_examples 'resource fields' do
  shared_examples 'confidential resource' do |field|
    context 'when the resource is confidential' do
      before do
        resource[:confidential] = true
      end

      describe "##{field}" do
        it 'returns CONFIDENTIAL_TEXT' do
          expect(subject.public_send(field))
            .to eq(described_class::CONFIDENTIAL_TEXT)
        end
      end
    end
  end

  if described_class.const_defined?(:FIELDS)
    describe 'FIELDS' do
      described_class::FIELDS.each do |field|
        describe "##{field}" do
          it 'returns the corresponding attribute' do
            expect(subject.public_send(field)).to eq(resource[field])
          end
        end

        include_examples('confidential resource', field)
      end
    end
  end

  if described_class.const_defined?(:DATE_FIELDS)
    describe 'DATE_FIELDS' do
      described_class::DATE_FIELDS.each do |field|
        describe "##{field}" do
          it 'returns a parsed date' do
            expect(subject.public_send(field)).to eq(public_send(field))
          end

          context "when #{field} is nil" do
            before do
              resource.delete(field)
            end

            it 'returns nil' do
              expect(subject.public_send(field)).to be_nil
            end
          end

          include_examples('confidential resource', field)
        end
      end
    end
  end

  if described_class.const_defined?(:TIME_FIELDS)
    describe 'TIME_FIELDS' do
      described_class::TIME_FIELDS.each do |field|
        describe "##{field}" do
          it 'returns a parsed time' do
            expect(subject.public_send(field)).to eq(public_send(field))
          end

          context "when #{field} is nil" do
            before do
              resource.delete(field)
            end

            it 'returns nil' do
              expect(subject.public_send(field)).to be_nil
            end
          end

          include_examples('confidential resource', field)
        end
      end
    end
  end
end
